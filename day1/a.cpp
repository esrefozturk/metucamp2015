#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;
int K;
int n,m,p[102][102];
int a,b,s;
int result;
int main()
{
	
	cin >> K;
	while(K--)
	{
		cin >> n >> m;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				p[i][j] = 0;
		for(int i=0 ;i<m;i++ )
		{
			cin >> a >> b;
			p[a][b] = 1;
			p[b][a] = 1;
		}
		cin >> s;
		result = 0;
		for(int i=1;i<=n;i++)
			if( i != s && p[i][s] )
				cout << i << " ";
		cout << endl;
	}
	return 0;
}
