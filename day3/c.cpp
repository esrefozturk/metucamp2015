#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>
#define ulli unsigned long long int

using namespace std;

int main()
{
	ulli n;

	cin >> n;

	for(int i=2;;i++)
	{
		if( n%i==0 )
		{
			cout << i << " " << ( n/i ) << endl;
			return 0;
		}

	}

	return 0;
}