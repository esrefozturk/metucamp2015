#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>
#define ulli unsigned long long int 

using namespace std;

int m ,n;

void solve( int a , int b , int c )
{
	//cout << a << " " << b << " " << c << endl;
	if( c > 2*a+2*b-4 )
	{
		m+=1;
		n+=1;
		if( a>2 && b>2 )
			solve(a-2,b-2,c-(2*a+2*b-4));
		return;
	}

	if( c<=b )
	{
		m+=1;
		n+=c;
	}
	else if( c <=a+b-1  )
	{
		m+=c-b+1;
		n+=b;
	}
	else if(c<=2*b+a-2)
	{
		m += a;
		n += 2*b+a-2-c+1;
	}
	else
	{
		m+= 2*a+2*b-3-c+1;
		n+= 1;
	}

}

int main()
{
	int a,b,c;

	cin >> a >>b >> c;

	solve(a,b,c);
	cout << m << " " << n << endl;



	return 0;
}