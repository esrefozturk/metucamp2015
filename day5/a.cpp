#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>
#define ulli unsigned long long int 
using namespace std;

int gcd(int a, int b)
{
	if(b==0)
		return a;
	return gcd(b,a%b);
}

int main()
{
	
	int n;
	ulli sum=0;
	int a,b;
	cin >> n;
	cin >> a;
	sum = a;
	for(int i=1;i<n;i++)
	{
		
		cin >> b;
		sum +=b;
		a = gcd(a,b);
		//out << a  << "  " << b << endl;
	}

	cout << (sum/a) << endl;

	return 0;
}