#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#define ulli unsigned long long int
using namespace std;


ulli n,m[25];
ulli r;


ulli fact( ulli  a)
{
	if(a<=1)
		return 1;
	return a*fact(a-1);
}


int main()
{
	cin >> n;
	
	for(int i=0;i<n;i++)
		{cin >> m[i]; r+=m[i];}

	r = fact(r);
	for(int i=0;i<n;i++)
	{
		r /= fact(m[i]);
	}

	cout << r << endl;

	return 0;
}